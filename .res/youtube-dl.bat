@echo off
title YouTube-DL by Nero
color A
youtube-dl.exe -U -q
FOR /F "tokens=* USEBACKQ" %%F IN (`youtube-dl.exe --version`) DO (
SET var=%%F
)

:Neustart
echo				        YouTube-DL v%var%
echo				  ***********************************
echo(
echo 1 = Audio (mp3)
echo 2 = Video (mp4)
echo 3 = Video (bestmoegliche Qualitaet)
echo 4 = Playlist (mp3)
echo 5 = Playlist (mp4)
echo(
SET /p modus="Wahl: "
title YouTube-DL by Nero [%modus%]
cls
echo				        YouTube-DL v%var%
echo				  ***********************************
echo(

IF "%modus%"=="1" goto mp3
IF "%modus%"=="2" goto mp4
IF "%modus%"=="3" goto auto
IF "%modus%"=="4" goto playlist_mp3
IF "%modus%"=="5" goto playlist_mp4

:mp3
	echo(
	SET /p tag="Link / Tag / Titel: "
	if "%tag%"=="0" cls && goto Neustart
	youtube-dl.exe --default-search "ytsearch" "%tag%" -q -R 5 --no-playlist -x --audio-format mp3 --audio-quality 0 -o "..\Downloads\%%(title)s.%%(ext)s" --exec 'echo "\nFolgender Titel wurde heruntergeladen:\n"{}'
	echo(
	goto mp3

:mp4
	echo(
	SET /p tag="Link / Tag / Titel: "
	if "%tag%"=="0" cls && goto Neustart
	youtube-dl.exe --default-search "ytsearch" "%tag%" -q -R 5 --no-playlist -o "..\Downloads\%%(title)s.%%(ext)s" --recode-video mp4 --exec 'echo "\nFolgender Titel wurde heruntergeladen:\n"{}'
	goto mp4

:auto
	echo(
	SET /p tag="Link / Tag / Titel: "
	if "%tag%"=="0" cls && goto Neustart
	youtube-dl.exe --default-search "ytsearch" "%tag%" -q -R 5 --no-playlist -o "..\Downloads\%%(title)s.%%(ext)s" --exec 'echo "\nFolgender Titel wurde heruntergeladen:\n"{}'
	goto auto

:playlist_mp3
	echo(
	SET /p tag="Link / Tag: "
	if "%tag%"=="0" cls && goto Neustart
	youtube-dl.exe %tag% -R 5 -x --audio-format mp3 --audio-quality 0 --yes-playlist -o "..\Downloads\%%(title)s.%%(ext)s"
	echo Playlist heruntergeladen!
	echo(
	goto playlist_mp3

:playlist_mp4
	echo(
	SET /p tag="Link / Tag: "
	if "%tag%"=="0" cls && goto Neustart
	youtube-dl.exe %tag% -R 5 --yes-playlist -o "..\Downloads\%%(title)s.%%(ext)s" --recode-video mp4
	echo Playlist heruntergeladen!
	echo(
	goto playlist_mp4